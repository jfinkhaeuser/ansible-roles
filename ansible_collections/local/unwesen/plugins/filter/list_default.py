# -*- coding: utf-8 -*-
"""Provide defaults for lists."""

from __future__ import absolute_import, annotations, division, print_function


__metaclass__ = type  # pylint: disable=C0103

from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from typing import Iterable, Any


DOCUMENTATION = """
    name: list_default
    author: Jens Finkhaeuser
    version_added: "1.0.0"
    short_description: Provide defaults for list entries
    description:
      - Modify and return a given list; if an item is set, return the item, otherwise return a default.
"""

EXAMPLES = """
# list_default filter example

- name: Return defaults for a list
  ansible.builtin.debug:
    msg: "{{ [42, None] | local.unwesen.list_default('hi')}}"
# -> [42, 'hi']
"""


def _list_default(value: Iterable[Any], default: Any) -> list[Any]:
  """Returns the list with missing/empty items replaced by a default value."""
  return [(v or default) for v in value]

class FilterModule(object):
  """Filter plugin."""

  def filters(self):
    return {
      'list_default' : _list_default,
    }
