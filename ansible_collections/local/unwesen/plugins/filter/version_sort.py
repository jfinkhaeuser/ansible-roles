# -*- coding: utf-8 -*-
"""Sort by version semantics."""

from __future__ import absolute_import, annotations, division, print_function


__metaclass__ = type  # pylint: disable=C0103

from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from typing import Iterable, Any


DOCUMENTATION = """
    name: version_sort
    author: Jens Finkhaeuser
    version_added: "1.0.0"
    short_description: Sort by version semantics
    description:
      - Sort by version semantics
"""

EXAMPLES = """
# TBD
"""


from looseversion import LooseVersion

def _version_sort(l: Iterable[Any]) -> Iterable[Any]:
  """Return the iterable sorted by version semantics"""
  return sorted(l, key=LooseVersion)

class FilterModule(object):
  """Filter plugin."""

  def filters(self):
    return {
      'version_sort' : _version_sort
    }
