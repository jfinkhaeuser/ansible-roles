#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: pihole

short_description: Adds adlists to pihole's gravity DB

author:
    - Jens Finkhaeuser <jens@finkhaeuser.de>
'''

EXAMPLES = r'''
- name: Test with a message
  sqlite:
    file: /path/to/gravity.db # default: /etc/pihole/gravity.db
    adlists:
    - url: https://adlist-url
      enabled: True|False
      comment: optional comment
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
added: 2
    description: Adlists added
    type: int
    returned: always
removed: 1
    description: Adlists removed
    type: int
    returned: always
updated: 3
    description: Adlists updated
    type: int
    returned: always
changed: True
    description: True if added > 0 or removed > 0. Updated does not affect this.
    type: bool
    returned: always
'''

from ansible.module_utils.basic import AnsibleModule

def _add_or_update(cursor, entry, result):
  statement = """SELECT id FROM adlist WHERE address = ?"""
  cursor.execute(statement, (entry['url'],))
  oldid = cursor.fetchall()

  statement = """INSERT INTO adlist (address, enabled, comment)
                    VALUES (?, ?, ?)
                    ON CONFLICT(address) DO
                    UPDATE SET enabled = excluded.enabled,
                               comment = excluded.comment
  """
  cursor.execute(statement, (entry['url'], entry['enabled'], entry['comment']))
  modified = cursor.fetchall()
  if len(oldid) <= 0:
    result['added'] += 1
  else:
    result['updated'] += 1
  return result


def _remove(cursor, entry, result):
  statement = """DELETE FROM adlist WHERE address = ?"""
  cursor.execute(statement, (entry['url'],))
  cursor.fetchall()
  result['removed'] += cursor.rowcount
  return result



def _run_script(file, adlists):
  ### First, create an sqlite3 connection and initialize the table (if it does
  ### not exist yet).
  import sqlite3
  connection = sqlite3.connect(file)
  cursor = connection.cursor()

  # TODO create_table parameter?
  cursor.execute("""CREATE TABLE IF NOT EXISTS adlist
      (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          address TEXT UNIQUE NOT NULL,
          enabled BOOLEAN NOT NULL DEFAULT 1,
          date_added INTEGER NOT NULL DEFAULT (cast(strftime('%s', 'now') as int)),
          date_modified INTEGER NOT NULL DEFAULT (cast(strftime('%s', 'now') as int)),
          comment TEXT,
          date_updated INTEGER,
          number INTEGER NOT NULL DEFAULT 0,
          invalid_domains INTEGER NOT NULL DEFAULT 0,
          status INTEGER NOT NULL DEFAULT 0
      );
  """)

  ### Next, iterate over the adlists and either add/update or remove each item
  ENTRY_DEFAULTS = list({
    "state": "present",
    "enabled": True,
    "comment": "",
  }.items())

  result = {
    'added': 0,
    'updated': 0,
    'removed': 0,
    'changed': False,
  }

  added = 0
  removed = 0
  for unmodified in adlists:
    entry = dict(ENTRY_DEFAULTS + list(unmodified.items()))
    if entry['state'].lower() == 'absent':
      result = _remove(cursor, entry, result)
    elif entry['state'].lower() == 'present':
      result = _add_or_update(cursor, entry, result)
    else:
      raise AnsibleError("The 'state' flag must be one of 'present' or 'absent'; the default is 'present'")

  connection.commit()

  if result['added'] > 0 or result['removed'] > 0:
    result['changed'] = True
  return result


def run_module():
  # define available arguments/parameters a user can pass to the module
  module_args = {
    'file': {
        'type': 'str',
        'required': False,
        'default': '/etc/pihole/gravity.db',
    },
    'adlists': {
      'type': 'list',
      'required': False,
      'default': [],
    },
  }

  # seed the result dict in the object
  # we primarily care about changed and state
  # changed is if this module effectively modified the target
  # state will include any data that you want your module to pass back
  # for consumption, for example, in a subsequent task
  result = {
    'added': 0,
    'updated': 0,
    'removed': 0,
    'changed': False,
  }

  # the AnsibleModule object will be our abstraction working with Ansible
  # this includes instantiation, a couple of common attr would be the
  # args/params passed to the execution, as well as if the module
  # supports check mode
  module = AnsibleModule(
    argument_spec = module_args,
    supports_check_mode = True
  )

  # if the user is working with this module in only check mode we do not
  # want to make any changes to the environment, just return the current
  # state with no modifications
  if module.check_mode:
    module.exit_json(**result)

  try:
    res = _run_script(**module.params)
    result.update(res)
  except RuntimeError as ex:
    module.fail_json(msg = ex.what(), **result)

  # in the event of a successful module execution, you will want to
  # simple AnsibleModule.exit_json(), passing the key/value results
  module.exit_json(**result)


def main():
  run_module()

if __name__ == '__main__':
  main()
