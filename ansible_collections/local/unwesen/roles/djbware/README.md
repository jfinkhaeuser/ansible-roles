djbware
-------

These roles set up various djb tools, though they're not all "djbware" per se.

- Some are from fehcom, which explicitly labels them as [djbware](http://www.fehcom.de/ipnet/djbware.html)
- Others are from e.g. [untroubled.org](http://untroubled.org/software.php)

For all, we try to follow the [slashpackage](https://cr.yp.to/slashpackage.html)
convention, which may mean any of the following:

1. Repacking the sources to contain only the `<package>-<version>` directory at
   the top level.
1. Implement some of the convention in the roles, e.g. linking of `<package>`
   to `<package>-<version>`.
1. etc.
