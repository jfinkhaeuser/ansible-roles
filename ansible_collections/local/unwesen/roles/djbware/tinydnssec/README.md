# Roles Overview

There are two *main* roles:

- `djbware/tinydnssec`, which installs and configures tinydnssec, and rolls out
  zone configuration.
- `djbware/tinydnssec/zones`, which just rolls out zone configuration.

These are implemented by a few supplementary roles.

- `djbware/tinydnssec/_build` performs the build
- `djbware/tinydnssec/_service` configures the services.
- `djbware/tinydnssec/_defs` contains definitions of default values.

There's a circula dependency between `_build` and `_service`, because the former
needs to stop the service to re-build, while the latter needs the binaries to
successfully configure the service.

We solve this by `_build` depending on `_service`, which may configure services
that keep failing. `_build` then stops the service, builds, and restarts it.
If there are already binaries installed, services configured by `_service`
will start.
