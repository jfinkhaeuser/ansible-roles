# Roles Overview

There are two *main* roles:

- `service/apache`, which installs and configures apache, and rolls out virtual
  hosts.
- `service/apache/vhosts`, which just rolls out virtual hosts.

These are implemented by a few supplementary roles.

- `service/apache/_base` does the base installation.
- `service/apache/_vhost` configures a *single* virtual host.
- `service/apache/_defs` contains definitions of default values.

In more detail, what this means is that:

- `service/apache` depends on:
  - `service/apache/_base` and
  - `service/apache/vhosts`
- Meanwhile, `service/apache/vhosts` loops over the `service_apache_vhosts` array,
  and invokes `service/apache/_vhost` for every entry.
- Both `service/apache/_base` and `service/apache/_vhost` depend on
  `service/apache/_defs` for defaults.
