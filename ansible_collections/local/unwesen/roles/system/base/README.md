Base host configuration. Should really be applied to any and all hosts.
This is an *empty* role, it just has dependencies to other roles. See
`meta/main.yml` for details.
