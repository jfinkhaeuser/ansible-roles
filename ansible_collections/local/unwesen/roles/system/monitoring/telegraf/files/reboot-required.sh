#!/bin/sh

echo "# TYPE reboot_required counter"
echo "# HELP reboot_required 1 if a reboot is required, otherwise 0. If a reboot is required, there'll be a message label."

if test -f /var/run/reboot-required ; then
  msg=$(cat /var/run/reboot-required)
  echo "reboot_required{msg=\"${msg}\"} 1"
else
  echo "reboot_required{msg=\"\"} 0"
fi
