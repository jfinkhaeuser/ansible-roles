#!/usr/bin/env python3

SYSCTL = '/usr/bin/systemctl'

FIELDS = (
#  'MemoryCurrent',
#  'CPUUsageNSec',
  'Id',
  'LoadState',
  'ActiveState',
  'SubState',
#  'ActiveEnterTimestamp',
  'StateChangeTimestamp',
)

def get_output(command):
  import subprocess
  out = subprocess.check_output(command)
  return out.decode('utf8').splitlines()

def get_services():
  out = get_output([SYSCTL, 'list-units', '--all', '--type=service'])
  import re
  services = []
  for line in out[1:]:
    line = re.sub(' +', '|', line)
    items = line.split('|')[1:5]
    if len(items) != 4:
      continue
    if not items[0].endswith('.service'):
      continue
    services.append(items)
  return services


def get_status(service):
  out = get_output([SYSCTL, 'show', '--no-page', service])
  lines = []
  for line in out:
    lines.append(line.split('=', 1))
  return dict(lines)


def filter_services(services, service_globs):
  import fnmatch
  retval = []
  for service in services:
    for glob in service_globs:
      if fnmatch.fnmatch(service[0], glob):
        retval.append(service)
        break
  return retval

def convert_time(time):
  from datetime import datetime
  return datetime.strptime(time, '%a %Y-%m-%d %H:%M:%S %Z')


def main(service_globs, *args, **kw):
  service_globs = service_globs.split(' ')

  services = get_services()
  filtered = filter_services(services, service_globs)

  from datetime import datetime, UTC
  now = datetime.now(UTC)

  print("""# TYPE systemd_status_since counter
# HELP systemd_status_since counter The number of seconds since the current status has been entered
""")

  for service in filtered:
    status = get_status(service[0])

    status = {k: v for k, v in status.items() if k in FIELDS}

    # active_enter = convert_time(status['ActiveEnterTimestamp'])
    # active_enter_since = (now - active_enter).total_seconds()

    state_change = convert_time(status['StateChangeTimestamp']).astimezone(UTC)
    state_change_since = (now - state_change).total_seconds()

    print(f'systemd_status_since{{name="{status["Id"]}",load="{status["LoadState"]}",active="{status["ActiveState"]}",sub="{status["SubState"]}"}} {state_change_since}')


if __name__ == '__main__':
  import sys
  globs = ['']
  if len(sys.argv) > 1:
    globs = sys.argv[1:]
  main(*globs)
