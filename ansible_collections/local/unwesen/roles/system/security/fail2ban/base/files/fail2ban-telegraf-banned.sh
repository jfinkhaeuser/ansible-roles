#!/bin/sh

echo "# TYPE banned_by_fail2ban counter"
echo "# HELP banned_by_fail2ban The counter is always 1 for every entry; we report the jail and IP address as labels."

BANNED=$(sudo /opt/venv/fail2ban/bin/fail2ban-client banned | sed "s:':\":g")

JAILS=$(echo "${BANNED}" | jq '.[] | keys[]')
for jail in $JAILS ; do
  jail=$(echo "${jail}" | sed 's/"//g')
  IPS=$(echo "${BANNED}" | jq ".[].${jail}" | grep -v '^null')
  test "${IPS}" = "[]" && continue
  for ip in $(echo "${IPS}" | jq '.[]') ; do
    ip=$(echo "${ip}" | sed 's/"//g')
    echo "fail2ban_banned{address=\"${ip}\",jail=\"${jail}\"} 1"
  done
done
