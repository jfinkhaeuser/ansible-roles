---
### Installation
- name: Install/update dependencies
  package:
    name:
    - whois
    - logrotate
    - jq
    state: latest
  tags: [system,security,fail2ban]

- name: Install/update fail2ban
  include_tasks: install.yml
# FIXME: do we want something like this?
#   when: util_srcversion_registry.fail2ban is changed
  tags: [system,security,fail2ban]

### Configuration

- name: Install static configuration files
  copy:
    src: "{{ item.key }}"
    dest: "{{ item.value }}"
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0644
  with_dict:
    logrotate: /etc/logrotate.d/fail2ban
    fail2ban.conf: /etc/fail2ban/fail2ban.conf
    jail.conf: /etc/fail2ban/jail.conf
    paths-arch.conf: /etc/fail2ban/paths-arch.conf
    paths-debian.conf: /etc/fail2ban/paths-debian.conf
    paths-freebsd.conf: /etc/fail2ban/paths-freebsd.conf
    paths-osx.conf: /etc/fail2ban/paths-osx.conf
    paths-common.conf: /etc/fail2ban/paths-common.conf
    paths-fedora.conf: /etc/fail2ban/paths-fedora.conf
    paths-opensuse.conf: /etc/fail2ban/paths-opensuse.conf
  tags: [system,security,fail2ban]

- name: Install local configuration overrides
  template:
    src: "{{ item.key }}"
    dest: "{{ item.value }}"
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0644
  with_dict:
    jail.local.j2: /etc/fail2ban/jail.local
  tags: [system,security,fail2ban]

- name: Create configuration directories
  file:
    path: "/etc/fail2ban/{{ item }}"
    state: directory
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0750
  loop:
  - filter.d
  - action.d
  tags: [system,security,fail2ban]

- name: Install filter(s)
  copy:
    src: filters/{{ item }}.conf
    dest: /etc/fail2ban/filter.d/{{ item }}.conf
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0644
  loop:
  # - 3proxy
  # - apache-auth
  # - apache-badbots
  # - apache-botsearch
  # - apache-common
  # - apache-fakegooglebot
  # - apache-modsecurity
  # - apache-nohome
  # - apache-noscript
  # - apache-overflows
  # - apache-pass
  # - apache-shellshock
  # - assp
  # - asterisk
  # - axfrdns
  - bandetect
  # - bitwarden
  # - botsearch-common
  # - centreon
  - common
  # - counter-strike
  # - courier-auth
  # - courier-smtp
  # - cyrus-imap
  # - directadmin
  # - domino-smtp
  # - dovecot
  # - dropbear
  # - drupal-auth
  # - ejabberd-auth
  # - exim-common
  # - exim
  # - exim-spam
  # - freeswitch
  # - froxlor-auth
  # - gitlab
  # - grafana
  # - groupoffice
  # - gssftpd
  # - guacamole
  # - haproxy-http-auth
  # - horde
  - iplog
  # - kerio
  # - lighttpd-auth
  # - mongodb-auth
  # - monit
  # - monitorix
  # - mssql-auth
  # - murmur
  # - mysqld-auth
  # - nagios
  # - named-refused
  # - nginx-bad-request
  # - nginx-botsearch
  # - nginx-http-auth
  # - nginx-limit-req
  # - nsd
  # - openhab
  # - openwebmail
  # - oracleims
  # - pam-generic
  # - perdition
  # - phpmyadmin-syslog
  # - php-url-fopen
  # - portsentry
  # - postfix
  # - proftpd
  # - pure-ftpd
  # - qmail
  # - recidive
  # - repeatoffender
  # - roundcube-auth
  # - scanlogd
  # - screensharingd
  # - selinux-common
  # - selinux-ssh
  # - sendmail-auth
  # - sendmail-reject
  # - sieve
  # - slapd
  # - softethervpn
  # - sogo-auth
  # - solid-pop3d
  # - squid
  # - squirrelmail
  - sshd
  # - stunnel
  # - suhosin
  # - tine20
  # - traefik-auth
  # - uwimap-auth
  # - vsftpd
  # - webmin-auth
  # - wuftpd
  # - xinetd-fail
  # - znc-adminlog
  # - zoneminder
  tags: [system,security,fail2ban]

- name: Install action(s)
  copy:
    src: actions/{{ item }}.conf
    dest: /etc/fail2ban/action.d/{{ item }}.conf
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0644
  loop:
  # - abuseipdb
  # - apf
  # - apprise
  # - badips
  # - blocklist_de
  # - bsd-ipfw
  # - cloudflare
  # - complain
  # - dshield
  # - dummy
  # - firewallcmd-allports
  # - firewallcmd-common
  # - firewallcmd-ipset
  # - firewallcmd-multiport
  # - firewallcmd-new
  # - firewallcmd-rich-logging
  # - firewallcmd-rich-rules
  # - helpers-common
  # - hostsdeny
  # - ipfilter
  # - ipfw
  - iplog
  # - iptables-allports
  # - iptables-common
  # - iptables
  # - iptables-ipset
  # - iptables-ipset-proto4
  # - iptables-ipset-proto6-allports
  # - iptables-ipset-proto6
  # - iptables-multiport
  # - iptables-multiport-log
  # - iptables-new
  # - iptables-xt_recent-echo
  # - mail-buffered
  # - mail
  - mail-whois-common
  - mail-whois
  - mail-whois-lines
  # - mynetwatchman
  # - netscaler
  # - nftables-allports
  # - nftables-common
  # - nftables
  # - nftables-multiport
  # - nginx-block-map
  # - npf
  # - nsupdate
  # - osx-afctl
  # - osx-ipfw
  # - pf
  # - route
  - sendmail-buffered
  - sendmail-common
  - sendmail
  - sendmail-geoip-lines
  - sendmail-whois
  - sendmail-whois-ipjailmatches
  - sendmail-whois-ipmatches
  - sendmail-whois-lines
  - sendmail-whois-matches
  # - shorewall
  # - shorewall-ipset-proto6
  # - symbiosis-blacklist-allports
  - ufw
  # - xarf-login-attack
  tags: [system,security,fail2ban]

### Start fail2ban
- name: Start fail2ban
  service:
    name: fail2ban
    state: started
    enabled: true
  tags: [system,security,fail2ban]

- name: Check log file size
  stat:
    path: /var/log/fail2ban.log
  register: system_security_fail2ban_log
  tags: [system,security,fail2ban]

# Reload or restart fail2ban - restart helps fix logrotate issues, but we don't
# want to do it without reason.

- name: Reload fail2ban
  service:
    name: fail2ban
    state: reloaded
    enabled: true
  when: system_security_fail2ban_log.stat.exists and system_security_fail2ban_log.stat.size > 0
  tags: [system,security,fail2ban]

- name: Restart fail2ban
  service:
    name: fail2ban
    state: restarted
    enabled: true
  when: system_security_fail2ban_log.stat.exists and system_security_fail2ban_log.stat.size <= 0
  tags: [system,security,fail2ban]

- name: Create telegraf configuration
  template:
    src: telegraf.conf.j2
    dest: /etc/telegraf/telegraf.d/fail2ban.conf
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0644
  notify:
    - Restart telegraf
  tags: [system,security,fail2ban]

- name: Ensure telegraf can invoke fail2ban-client
  template:
    src: sudoers-telegraf-fail2ban.conf.j2
    dest: /etc/sudoers.d/telegraf-fail2ban
    owner: root
    group: "{{ system_admingroup }}"
    mode: 0644
  notify:
    - Restart telegraf
  tags: [system,security,fail2ban]

- name: Add monitoring of banned IP addresses
  copy:
    src: "fail2ban-telegraf-banned.sh"
    dest: "{{ system_monitoring_telegraf_exec_d_path }}/fail2ban-banned.sh"
    owner: "{{ system_monitoring_telegraf_user }}"
    group: "{{ system_admingroup }}"
    mode: 0775
  tags: [system,security,fail2ban]

- name: Ensure telegraf knows about our venv
  lineinfile:
    path: /etc/default/telegraf
    regexp: '^PATH="{{ python_venv_basedir }}/{{ system_security_fail2ban_venv }}/bin:/usr/bin:\$PATH"$'
    line: PATH="{{ python_venv_basedir }}/{{ system_security_fail2ban_venv }}/bin:/usr/bin:$PATH"
  notify:
    - Restart telegraf
  tags: [system,security,fail2ban]
