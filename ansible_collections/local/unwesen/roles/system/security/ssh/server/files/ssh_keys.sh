#!/bin/bash
set -e

create=0
if [ ! -f /etc/ssh/ssh_host_rsa_key ] ; then
  create=1
fi

bits=$(openssl rsa -text -noout -in /etc/ssh/ssh_host_rsa_key | grep ^Private | sed 's/.*(\([0-9][0-9]*\).*/\1/g')
echo "RSA host key has $bits bits"
if [ $bits -lt 4096 ] ; then
  create=1
fi

if [ $create -gt 0 ] ; then
  echo "(Re-)creating host keys..."
  rm /etc/ssh/ssh_host_*key*
  ssh-keygen -N '' -t ed25519 -f /etc/ssh/ssh_host_ed25519_key
  ssh-keygen -N '' -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key
  exit 1
fi

echo "Host keys are fine, nothing to do."
exit 0
