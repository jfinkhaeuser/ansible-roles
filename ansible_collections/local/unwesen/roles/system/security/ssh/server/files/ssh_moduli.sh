#!/bin/bash
changed=0
create=0

if [ ! -f /etc/ssh/moduli ] ; then
  create=1
else
  echo "Stripping bad moduli..."
  /usr/bin/awk '$5 > 3071' /etc/ssh/moduli >/tmp/moduli
  old_lines=$(wc -l /etc/ssh/moduli | cut -d' ' -f1)
  lines=$(wc -l /tmp/moduli | cut -d' ' -f1)
  if [ $lines -gt 0 ] ; then
    if [ $lines -lt $old_lines ] ; then
      echo "Using stripped moduli file!"
      mv /tmp/moduli /etc/ssh/moduli
      changed=1
    else
      echo "Nothing to do."
      rm /tmp/moduli
    fi
  else
    echo "Stripping emptied moduli file, need to recreate!"
    rm -f /etc/ssh/moduli
    create=1
  fi
fi

if [ $create -gt 0 ] ; then
  echo "(Re-)create moduli file..."
  ssh-keygen -G /etc/ssh/moduli.all -b 4096
  ssh-keygen -T /etc/ssh/moduli.safe -f /etc/ssh/moduli.all
  mv /etc/ssh/moduli.safe /etc/ssh/moduli
  rm /etc/ssh/moduli.all
  changed=1
fi

chmod 0644 /etc/ssh/moduli
exit $changed
